// ignore_for_file: deprecated_member_use
import 'package:client/api_service/catalog_service.dart';
import 'package:client/constants.dart';
import 'package:client/screens/home/bottom_navbar.dart';
import 'package:client/screens/payment/payment_page.dart';
import 'package:client/styles.dart';
import 'package:flutter/material.dart';
import '../../screens/bookViewer/pdf_viewer.dart';

class DetailBook extends StatefulWidget {
  const DetailBook({
    Key? key,
    required this.index,
  }) : super(key: key);

  final int index;

  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<DetailBook> {
  var jsonBooks = [];
  String title = "";
  String image = "";
  String content = "";
  String desc = "";
  List<dynamic> category = [];
  String author = "";
  int price = 0;
  bool buy = false;
  int id = 0;
  double rating = 4.5;
  int progress = 0;
  String publisher = "";
  String isbn = "";

  final BookService bookService = BookService();

  void initData() async {
    final detailBook = await bookService.getBooksById(widget.index);

    setState(() {
      jsonBooks = jsonBooks;
      title = detailBook.data["title"];
      image = detailBook.data["cover"];
      content = detailBook.data["content"];
      desc = detailBook.data["description"];
      category = detailBook.data["category"];
      author = detailBook.data["author"];
      price = detailBook.data["price"];
      buy = detailBook.data["isOwned"];
      id = detailBook.data["id"];
      progress = detailBook.data["progress"];
      publisher = detailBook.data["publisher"];
      isbn = detailBook.data["isbn"];
    });
  }

  @override
  void initState() {
    super.initState();
    initData();
  }

  @override
  Widget build(BuildContext context) {
    Widget detailBook() {
      return ListView(children: [
        Container(
            alignment: Alignment.center,
            child: Column(children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: image.isEmpty
                    ? Container()
                    : Image.network(
                        "$BASE_URL/file/$image",
                        width: 150,
                      ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 20),
                width: 250,
                alignment: Alignment.center,
                child: Text(
                  "Author : " + author,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      fontFamily: "Poppins",
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Color.fromRGBO(0, 0, 0, 0.5)),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 10),
                width: 250,
                alignment: Alignment.center,
                child: Text(
                  "Publisher : " + publisher,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      fontFamily: "Poppins",
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Color.fromRGBO(0, 0, 0, 0.5)),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 10),
                width: 250,
                alignment: Alignment.center,
                child: Text(
                  "ISBN : " + isbn,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontFamily: "Poppins",
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Color.fromRGBO(0, 0, 0, 0.5),
                  ),
                ),
              ),
              SizedBox(
                  width: category.length > 1 ? 200 : 105,
                  child: category.length > 1
                      ? Wrap(
                          alignment: WrapAlignment.center,
                          spacing: 5,
                          children: [
                            for (int i = 0; i < category.length; i++)
                              Container(
                                margin: const EdgeInsets.only(top: 10),
                                padding: const EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 12),
                                decoration: BoxDecoration(
                                  color: blueLight,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Text(
                                  category[i],
                                  style: const TextStyle(
                                      fontFamily: "Poppins",
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white),
                                ),
                              ),
                          ],
                        )
                      : Container(
                          margin: const EdgeInsets.only(top: 10),
                          padding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 12),
                          decoration: BoxDecoration(
                            color: blueLight,
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Text(
                            category.isEmpty ? "No category" : category[0],
                            style: const TextStyle(
                                fontFamily: "Poppins",
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                                color: Colors.white),
                            textAlign: TextAlign.center,
                          ),
                        )),
              Container(
                  margin: const EdgeInsets.only(top: 10),
                  alignment: Alignment.center,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                            margin: const EdgeInsets.only(right: 10),
                            child:
                                Image.asset("assets/icon/star.png", width: 15)),
                        Text(
                          rating.toString(),
                          style: const TextStyle(
                              fontFamily: "Poppins",
                              fontSize: 12,
                              fontWeight: FontWeight.w600),
                        ),
                      ])),
              Container(
                  margin: const EdgeInsets.only(top: 30),
                  padding:
                      const EdgeInsets.symmetric(vertical: 25, horizontal: 25),
                  decoration: BoxDecoration(
                    color: backgroundColor,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25)),
                    boxShadow: const [
                      BoxShadow(
                          color: Colors.black38,
                          spreadRadius: 0,
                          blurRadius: 1),
                    ],
                  ),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            "Synopsis",
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontSize: 16,
                                fontWeight: FontWeight.w700),
                          ),
                          buy == true
                              ? Text(
                                  "You already have this!",
                                  style: TextStyle(
                                      fontFamily: "Poppins",
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      color: blueDark),
                                )
                              : price == 0
                                  ? Text(
                                      "Free",
                                      style: TextStyle(
                                          fontFamily: "Poppins",
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          color: blueDark),
                                    )
                                  : Text(
                                      "Rp" + price.toString() + ",-",
                                      style: TextStyle(
                                          fontFamily: "Poppins",
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          color: blueDark),
                                    )
                        ],
                      ),
                      Container(
                          margin: const EdgeInsets.only(top: 30),
                          child: Text(
                            desc,
                            style: const TextStyle(
                              fontFamily: "Poppins",
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                            ),
                            textAlign: TextAlign.justify,
                          )),
                    ],
                  ))
            ]))
      ]);
    }

    return Scaffold(
      backgroundColor: const Color(0xFFF9F8F8),
      appBar: AppBar(
          automaticallyImplyLeading: false,
          toolbarHeight: 100,
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            padding: const EdgeInsets.only(left: 24, top: 0),
            icon: Image.asset(
              'assets/icon/left-arrow-bold.png',
              width: 22,
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => BottomNavbar()));
            },
          ),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                flex: 5,
                child: Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.only(right: 50),
                    child: Text(
                      title,
                      textAlign: TextAlign.center,
                      maxLines: 2,
                      style: const TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    )),
              )
            ],
          )),
      body: detailBook(),
      bottomNavigationBar: buy == true
          ? Container(
              // alignment: Alignment.bottomCenter,
              margin: const EdgeInsets.only(left: 80, right: 80),
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, readRoute,
                          arguments: PDFViewerArgument(
                              content: content,
                              title: title,
                              bookId: id,
                              progress: progress));
                    },
                    color: blueLight,
                    textColor: Colors.white,
                    child: const Text('Read now')),
              ))
          : Container(
              // alignment: Alignment.bottomCenter,
              margin: const EdgeInsets.only(left: 80, right: 80),
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PaymentPage(
                                  index: id,
                                  buy: buy,
                                  category: category[0],
                                  title: title,
                                  image: image,
                                  price: price,
                                  rating: rating,
                                  author: author)));
                    },
                    color: blueLight,
                    textColor: Colors.white,
                    child: const Text('Buy now')),
              )),
    ); // This trailing comma makes auto-formatting nicer for build method.
  }
}
