import 'package:client/api_service/catalog_service.dart';
import 'package:client/components/screens/resultSearch/result_filter.dart';
import 'package:client/model/category.dart';
import 'package:client/styles.dart';
import 'package:flutter/material.dart';
import 'package:filter_list/filter_list.dart';

List<Category> userList = [
  const Category(id: 1, name: "Comic"),
  const Category(id: 2, name: "Novel"),
  const Category(id: 3, name: "Education"),
  const Category(id: 4, name: "Children"),
  const Category(id: 5, name: "Technology"),
];

class Filterpage extends StatefulWidget {
  const Filterpage({Key? key}) : super(key: key);

  @override
  _FilterpageState createState() => _FilterpageState();
}

class _FilterpageState extends State<Filterpage> {
  // const _FilterpageState({Key? key, this.selectedUserList}) : super(key: key);
  List<Category> selectedUserList = [];
  List<Category> listCategory = [];

  final CatalogService catalogService = CatalogService();
  void initData() async {
    final homeData = (await catalogService.getHomePageData()).data;
    listCategory = homeData.categories;
    setState(() {
      listCategory = listCategory;
    });
    userList = listCategory;
  }

  @override
  void initState() {
    super.initState();
    initData();
  }

  @override
  Widget build(BuildContext context) {
    Widget back() {
      return Container(
        alignment: Alignment.centerLeft,
        padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 15),
        child: IconButton(
          icon: const Icon(Icons.arrow_back),
          color: Colors.white,
          tooltip: 'Back To Home',
          onPressed: () {
            Navigator.of(context).pop(true);
          },
          iconSize: 50,
        ),
      );
    }

    Widget title() {
      return Container(
          alignment: Alignment.centerLeft,
          padding: const EdgeInsets.only(left: 38.0, top: 10, bottom: 10),
          child: Text(
            "Filter",
            style: titleFilter,
            textAlign: TextAlign.left,
          ));
    }

    return Scaffold(
        body: Container(
            height: 1000,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: <Color>[
                  Color.fromARGB(242, 0, 128, 255),
                  Color.fromARGB(110, 0, 128, 255),
                ], // red to yellow
                tileMode:
                    TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
            child: Column(children: [
              const SizedBox(
                height: 80,
              ),
              back(),
              title(),
              Expanded(
                  child: Container(
                padding: const EdgeInsets.only(left: 20.0, top: 5),
                child: FilterListWidget<Category>(
                  height: 400,
                  selectedChipTextStyle: titlefilter,
                  unselectedChipTextStyle: titlefilter,
                  controlButtonTextStyle: titlefilter,
                  applyButtonTextStyle: applytext,
                  applyButonTextBackgroundColor:
                      const Color.fromARGB(255, 255, 255, 255),
                  backgroundColor: const Color.fromARGB(0, 255, 0, 0),
                  selectedTextBackgroundColor: blueDark,
                  unselectedTextbackGroundColor: backgroundfilter,
                  hideHeader: true,
                  hideSelectedTextCount: true,
                  listData: userList,
                  enableOnlySingleSelection: false,
                  selectedListData: selectedUserList,
                  onApplyButtonClick: (list) {
                    list!.isNotEmpty
                        ? Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    ResultFilter(category: list)))
                        // showDialog(
                        //     context: context,
                        //     builder: (BuildContext ctx) => AlertDialog(
                        //           title: const Text("Category: "),
                        //           content: Text(list[1].name),
                        //           actions: <Widget>[
                        //             TextButton(
                        //                 onPressed: () => Navigator.pop(ctx),
                        //                 child: const Text("Ok"))
                        //           ],
                        //         ))
                        : showDialog(
                            context: context,
                            builder: (BuildContext ctx) => AlertDialog(
                                  title: const Text("Category: "),
                                  content: const Text(
                                      "Please choose one of the category"),
                                  actions: <Widget>[
                                    TextButton(
                                        onPressed: () => Navigator.pop(ctx),
                                        child: const Text("Ok"))
                                  ],
                                ));
                  },
                  choiceChipLabel: (item) {
                    /// Used to display text on chip
                    return item!.name;
                  },
                  validateSelectedItem: (list, val) {
                    ///  identify if item is selected or not
                    return list!.contains(val);
                  },
                  onItemSearch: (user, query) {
                    // ignore: null_check_always_fails
                    return null!;
                  },
                ),
              ))
            ])));
  }
}
