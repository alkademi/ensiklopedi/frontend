import 'package:client/api_service/catalog_service.dart';
import 'package:client/components/screens/filter/filter_page.dart';
import 'package:client/components/screens/resultSearch/element_result.dart';
import 'package:client/model/book.dart';
import 'package:client/screens/home/bottom_navbar.dart';
import 'package:client/styles.dart';
import 'package:flutter/material.dart';

String word = '';

class ResultSearch extends StatefulWidget {
  const ResultSearch({
    Key? key,
    required this.query,
  }) : super(key: key);

  final String query;
  @override
  _ResultSearchState createState() => _ResultSearchState();
}

class _ResultSearchState extends State<ResultSearch> {
  late ScrollController _controller;
  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        //you can do anything here
      });
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        //you can do anything here
      });
    }
  }

  List<Book2> jsonBook = [];
  final SearchService searchService = SearchService();
  void initData() async {
    final searchData =
        (await searchService.getSearchPageData(widget.query)).data;
    jsonBook = searchData.books;

    setState(() {
      jsonBook = jsonBook;
    });
  }

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener); //the listener for up and down.
    super.initState();
    initData();
  }

  @override
  Widget build(BuildContext context) {
    //FITUR SEARCH
    Widget searchField() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Flexible(
            child: TextField(
              onChanged: (value) => (setState(
                () => word = value,
              )),
              decoration: InputDecoration(
                prefixIcon: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => BottomNavbar()));
                  },
                  child: Container(
                      padding: const EdgeInsets.all(10),
                      child: const Icon(Icons.arrow_back, color: Colors.black)),
                ),
                hintText: 'Find your favorite books',
                fillColor: searchBackground,
                filled: true,
                hintStyle: placeholderText,
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide.none),
                suffixIcon: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ResultSearch(
                                  query: word,
                                )));
                  },
                  child: Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: blueDark,
                          borderRadius: BorderRadius.circular(15)),
                      child: const Icon(Icons.search_rounded,
                          color: Colors.white)),
                ),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const Filterpage()));
            },
            child: Container(
                margin: const EdgeInsets.only(left: 10),
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: backgroundColor,
                    borderRadius: BorderRadius.circular(15)),
                child: const Icon(Icons.widgets_outlined, color: Colors.black)),
          ),
        ],
      );
    }

    // ignore: non_constant_identifier_names
    Widget ListResult2() {
      return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: ListView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            padding: const EdgeInsets.symmetric(horizontal: 0),
            itemCount: jsonBook.length,
            itemBuilder: (BuildContext context, int index) {
              return ElementBook(
                id: jsonBook[index].id,
                image: jsonBook[index].cover,
                title: jsonBook[index].title,
                author: jsonBook[index].author,
                category: jsonBook[index].category,
                rating: 4.5,
              );

              // return Container();
            }),
      );
    }

    return Scaffold(
      backgroundColor: backgroundColor,
      body: ListView(
        children: [
          Container(
              padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 25),
              decoration: BoxDecoration(color: backgroundColor),
              child: Column(
                verticalDirection: VerticalDirection.down,
                children: [
                  searchField(),
                  Container(
                      alignment: Alignment.centerLeft,
                      margin: const EdgeInsets.only(top: 20),
                      padding: const EdgeInsets.only(
                          left: 15.0, top: 10, bottom: 10),
                      child: Text(
                        "Result for " '"' + widget.query + '"',
                        style: titleSearch,
                        textAlign: TextAlign.left,
                      )),
                  jsonBook.isEmpty
                      ? SizedBox(
                          height: 500,
                          child: Center(
                              child: Text("There is no book",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: textSubHeading))))
                      : ListResult2()
                ],
              )),
        ],
      ),
    );
  }
}
