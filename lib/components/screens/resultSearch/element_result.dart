import 'package:client/components/detail/detail_book.dart';
import 'package:client/model/category.dart';
// import 'package:client/components/screens/filter/filterPage.dart';
// import 'package:client/screens/login/LoginPage.dart';
import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../../styles.dart';

class ElementBook extends StatefulWidget {
  const ElementBook({
    Key? key,
    required this.id,
    required this.image,
    required this.title,
    required this.author,
    required this.category,
    required this.rating,
  }) : super(key: key);

  final int id;
  final String image;
  final String title;
  final String author;
  final List<String> category;
  final double rating;

  @override
  State<ElementBook> createState() => _ElementBookState();
}

class _ElementBookState extends State<ElementBook> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => {
        // print(title),
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailBook(index: widget.id)))
      },
      child: Container(
        margin: const EdgeInsets.only(top: 20),
        height: widget.category.length == 1 ? 180 : 230,
        padding: const EdgeInsets.all(10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(style: BorderStyle.none),
            boxShadow: const [
              BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 1),
            ]),
        child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.only(left: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network("$BASE_URL/file/${widget.image}",
                      width: 105),
                ),
                const SizedBox(width: 20),
                widget.category.length > 1
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SizedBox(
                              width: 150,
                              child: Text(
                                widget.title,
                                maxLines: 2,
                                style: const TextStyle(
                                  fontFamily: 'Poppins',
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              )),
                          SizedBox(
                              width: 150,
                              child: Text(
                                widget.author,
                                maxLines: 2,
                                style: const TextStyle(
                                  fontFamily: 'Poppins',
                                  fontSize: 14,
                                  color: Colors.grey,
                                ),
                              )),
                          Row(
                            children: [
                              const Icon(
                                Icons.star_rate_rounded,
                                color: Color.fromARGB(255, 255, 187, 0),
                                size: 20.0,
                                semanticLabel: 'Ratings',
                              ),
                              Text(
                                widget.rating.toString(),
                                style: bookTitle.copyWith(fontSize: 13),
                                textAlign: TextAlign.start,
                              ),
                            ],
                          ),
                          SizedBox(
                              width: 200,
                              child: Wrap(runSpacing: 5, children: [
                                for (int i = 0; i < widget.category.length; i++)
                                  (Container(
                                      margin: const EdgeInsets.only(right: 5),
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8, horizontal: 15),
                                      decoration: BoxDecoration(
                                        color: blueLight,
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      child: Text(
                                        widget.category[i],
                                        style: bookTitle.copyWith(
                                            fontSize: 11, color: Colors.white),
                                        textAlign: TextAlign.start,
                                      ))),
                              ]))
                        ],
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                            SizedBox(
                                width: 150,
                                child: Text(
                                  widget.title,
                                  maxLines: 2,
                                  style: const TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                )),
                            SizedBox(
                                width: 150,
                                child: Text(
                                  widget.author,
                                  maxLines: 2,
                                  style: const TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 14,
                                    color: Colors.grey,
                                  ),
                                )),
                            SizedBox(
                              width: 200,
                              child: Row(children: [
                                for (int i = 0; i < widget.category.length; i++)
                                  (Container(
                                      margin: const EdgeInsets.only(right: 5),
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8, horizontal: 15),
                                      decoration: BoxDecoration(
                                        color: blueLight,
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      child: Text(
                                        widget.category[i],
                                        style: bookTitle.copyWith(
                                            fontSize: 11, color: Colors.white),
                                        textAlign: TextAlign.start,
                                      ))),
                                const SizedBox(width: 10),
                                Row(
                                  children: [
                                    const Icon(
                                      Icons.star_rate_rounded,
                                      color: Color.fromARGB(255, 255, 187, 0),
                                      size: 20.0,
                                      semanticLabel: 'Ratings',
                                    ),
                                    Text(
                                      widget.rating.toString(),
                                      style: bookTitle.copyWith(fontSize: 13),
                                      textAlign: TextAlign.start,
                                    ),
                                  ],
                                ),
                              ]),
                            )
                          ])
              ],
            )),
      ),
    );
  }
}
