import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color backgroundColor = const Color(0xFFFFFFFF);
Color blueDark = const Color(0xFF0068D0);
Color blueLight = const Color(0xFF76ABDF);
Color textHeading = const Color(0xFF000000);
Color textSubHeading = const Color(0xFF808080);
Color searchBackground = const Color(0xFFF3F1F1);
Color backgroundfilter = const Color.fromARGB(255, 118, 171, 223);
LinearGradient filterBackground =
    const LinearGradient(colors: <Color>[Color(0xFF007FFF), Color(0xFF76ABDF)]);

TextStyle titleText = GoogleFonts.poppins(
    fontSize: 24, fontWeight: FontWeight.w700, color: blueDark);

TextStyle subTitleText =
    GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w700);

TextStyle subheadingText = GoogleFonts.poppins(
    fontSize: 14, fontWeight: FontWeight.w600, color: textHeading);

TextStyle placeholderText =
    GoogleFonts.poppins(fontSize: 12, fontWeight: FontWeight.w500);

TextStyle seeMoreText = GoogleFonts.poppins(
    fontSize: 10, color: backgroundColor, fontWeight: FontWeight.w600);

TextStyle bookTitle =
    GoogleFonts.poppins(fontSize: 10, fontWeight: FontWeight.w700);

TextStyle categoryText = GoogleFonts.poppins(
    fontSize: 13, fontWeight: FontWeight.w600, color: backgroundColor);

TextStyle priceText = GoogleFonts.poppins(
    fontSize: 11, fontWeight: FontWeight.w600, color: textHeading);

TextStyle titlefilter = GoogleFonts.poppins(
    fontSize: 18, fontWeight: FontWeight.w600, color: Colors.white);

TextStyle applytext = GoogleFonts.poppins(
    fontSize: 18,
    fontWeight: FontWeight.w600,
    color: const Color.fromARGB(255, 0, 0, 0));

TextStyle titleFilter = GoogleFonts.poppins(
    fontSize: 48, fontWeight: FontWeight.w700, color: Colors.white);

TextStyle titleSearch = GoogleFonts.poppins(
    fontSize: 18, fontWeight: FontWeight.w600, color: Colors.black);

TextStyle textauthor = GoogleFonts.poppins(
    fontSize: 16,
    fontWeight: FontWeight.w600,
    color: const Color.fromARGB(125, 0, 0, 0));

TextStyle paymentText = GoogleFonts.poppins(
    fontSize: 24,
    fontWeight: FontWeight.w700,
    color: const Color.fromARGB(125, 0, 0, 0));
TextStyle buttonPaymentText = GoogleFonts.poppins(
    fontSize: 14,
    fontWeight: FontWeight.w600,
    color: const Color.fromARGB(125, 0, 0, 0));
