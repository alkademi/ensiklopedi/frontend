class Category {
  final String name;
  final int id;

  const Category({
    required this.name,
    required this.id,
  });

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(
      name: json["name"],
      id: json["id"],
    );
  }
}
