class ResponseBase<T> {
  final int status;
  final T data;
  final String message;

  const ResponseBase({
    required this.status,
    required this.data,
    required this.message,
  });

  factory ResponseBase.fromJson(Map<String, dynamic> json, T data) {
    return ResponseBase(
        status: json["status"], message: json["message"], data: data);
  }
}
