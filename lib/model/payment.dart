// ignore_for_file: non_constant_identifier_names

class Metadata {
  final String payment_method;

  const Metadata({required this.payment_method});

  factory Metadata.fromJson(Map<String, dynamic> json) {
    return Metadata(
      payment_method: json["payment_method"],
    );
  }
}

class MetadataPost {
  final String phone_number;

  const MetadataPost({required this.phone_number});
}
