class Collection {
  final int idBook;
  final int id;
  final int progress;
  final BookCollection book;

  const Collection({
    required this.idBook,
    required this.id,
    required this.progress,
    required this.book,
  });

  factory Collection.fromJson(Map<String, dynamic> json) {
    return Collection(
      idBook: json["id_book"],
      id: json["id"],
      progress: json["progress"],
      book: BookCollection.fromJson(json["book"]),
    );
  }
}

class BookCollection {
  final String title;
  final int id;
  final String author;
  final String cover;
  final String content;

  const BookCollection({
    required this.title,
    required this.id,
    required this.author,
    required this.cover,
    required this.content,
  });

  factory BookCollection.fromJson(Map<String, dynamic> json) {
    return BookCollection(
      title: json["title"],
      id: json["id"],
      author: json["author"],
      cover: json["cover"],
      content: json["content"],
    );
  }
}
