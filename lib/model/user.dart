class User {
  final String name;
  final String email;
  final int id;
  final int balance;
  final bool isAdmin;

  const User({
    required this.name,
    required this.email,
    required this.id,
    required this.balance,
    required this.isAdmin,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      name: json["name"],
      email: json["email"],
      id: json["id"],
      balance: json["balance"],
      isAdmin: json["isAdmin"],
    );
  }
}
