class Book {
  final String title;
  final int price;
  final String author;
  final String description;
  final String cover;
  final String content;
  final bool isOwned;
  final int id;
  final List<String> categories;
  final int progress;
  final String publisher;
  final String isbn;

  const Book(
      {required this.title,
      required this.price,
      required this.author,
      required this.description,
      required this.cover,
      required this.content,
      required this.isOwned,
      required this.id,
      required this.categories,
      required this.progress,
      required this.publisher,
      required this.isbn});

  factory Book.fromJson(Map<String, dynamic> json) {
    List<String> categories = [];
    dynamic categoriesJson = json["category"];
    for (int i = 0; i < categoriesJson.length; i++) {
      categories.add(categoriesJson[i]);
    }
    return Book(
      title: json["title"],
      price: json["price"],
      author: json["author"],
      description: json["description"],
      cover: json["cover"],
      content: json["content"],
      isOwned: json["isOwned"],
      id: json["id"],
      categories: categories,
      progress: json["progress"],
      publisher: json["publisher"],
      isbn: json["isbn"],
    );
  }
}

class Book2 {
  final String title;
  final int price;
  final String author;
  final String cover;
  // ignore: non_constant_identifier_names
  final bool IsOwned;
  final int id;
  final List<String> category;

  const Book2({
    required this.title,
    required this.price,
    required this.author,
    required this.cover,
    // ignore: non_constant_identifier_names
    required this.IsOwned,
    required this.id,
    required this.category,
  });

  factory Book2.fromJson(Map<String, dynamic> json) {
    List<String> categories = [];
    dynamic categoriesJson = json["category"];
    for (int i = 0; i < categoriesJson.length; i++) {
      categories.add((categoriesJson[i]));
    }
    return Book2(
      title: json["title"],
      price: json["price"],
      author: json["author"],
      cover: json["cover"],
      IsOwned: json["isOwned"],
      id: json["id"],
      category: categories,
    );
    // category: json["category"]);
  }
}

class Book3 {
  final String title;
  final int price;
  final String author;
  final String description;
  final String cover;
  final String content;
  final int id;
  final List<String> category;

  const Book3({
    required this.title,
    required this.price,
    required this.author,
    required this.cover,
    required this.description,
    required this.content,
    required this.id,
    required this.category,
  });

  factory Book3.fromJson(Map<String, dynamic> json) {
    List<String> categories = [];
    dynamic categoriesJson = json["category"];
    for (int i = 0; i < categoriesJson.length; i++) {
      categories.add((categoriesJson[i]));
    }
    return Book3(
      title: json["title"],
      price: json["price"],
      author: json["author"],
      cover: json["cover"],
      content: json["content"],
      description: json["description"],
      id: json["id"],
      category: categories,
    );
    // category: json["category"]);
  }
}
