// ignore: constant_identifier_names
const String BASE_URL = "http://192.168.1.15:8080/api/v1";
// ignore: constant_identifier_names
const String EMAIL_REGEX =
    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";

const splashRoute = "splash";
const loginRoute = "login";
const registerRoute = "register";
const homeRoute = "home";
const filterRoute = "filter";
const readRoute = "read";
