// ignore_for_file: non_constant_identifier_names

import 'package:client/api_service/api_base.dart';
import 'package:client/constants.dart';
import 'package:client/model/book.dart';
import 'package:client/model/payment.dart';
import 'package:client/model/response_base.dart';

class PaymentResponse {
  final int amount;
  final String email;
  final Book3 book;
  final String status;
  final Metadata metadata;

  const PaymentResponse({
    required this.amount,
    required this.email,
    required this.book,
    required this.status,
    required this.metadata,
  });

  factory PaymentResponse.fromJson(Map<String, dynamic> json) {
    return PaymentResponse(
        amount: json["amount"],
        email: json["email"],
        book: Book3.fromJson(json["book"]),
        status: json["status"],
        metadata: Metadata.fromJson(json["metadata"]));
  }
}

class PaymentService {
  final API api = const API(baseUrl: BASE_URL);

  Future<ResponseBase<PaymentResponse>> buy({
    required int book_id,
    required String payment_method,
    required String phoneNumber,
  }) async {
    try {
      dynamic response = await api.post("transaction/buy", body: {
        "book_id": book_id,
        "payment_method": payment_method,
        "mobile_number": phoneNumber,
      });

      return ResponseBase.fromJson(
          response, PaymentResponse.fromJson(response["data"]));
    } catch (e) {
      rethrow;
    }
  }
}
