import 'package:client/api_service/api_base.dart';
import 'package:client/constants.dart';
import 'package:client/model/response_base.dart';
import 'package:client/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginResponse {
  final User user;
  final String token;

  const LoginResponse({
    required this.user,
    required this.token,
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(
      user: User.fromJson(json["user"]),
      token: json["token"],
    );
  }
}

typedef RegisterResponse = User;

class UserResponse {
  final String name;
  final String email;
  final int balance;

  const UserResponse(
    this.name,
    this.email,
    this.balance,
  );

  factory UserResponse.fromJson(Map<String, dynamic> json) {
    String name = json["name"];
    String email = json["email"];
    int balance = json["balance"];
    return UserResponse(name, email, balance);
  }
}

class UserService {
  final API api = const API(baseUrl: BASE_URL);

  Future<ResponseBase<UserResponse>> fetchUser() async {
    final pref = await SharedPreferences.getInstance();
    const key = 'userData';
    final readData = pref.getInt(key);
    try {
      dynamic response = await api.get("user/" + readData.toString());
      return ResponseBase.fromJson(
          response, UserResponse.fromJson(response["data"]));
    } catch (e) {
      rethrow;
    }
  }

  Future<ResponseBase<LoginResponse>> login(
      String email, String password) async {
    try {
      dynamic response = await api
          .post("user/login", body: {"email": email, "password": password});
      var result = ResponseBase.fromJson(
          response, LoginResponse.fromJson(response["data"]));
      var pref = await SharedPreferences.getInstance();
      pref.setString("token", result.data.token);
      return ResponseBase.fromJson(
          response, LoginResponse.fromJson(response["data"]));
    } catch (e) {
      rethrow;
    }
  }

  Future<ResponseBase<RegisterResponse>> register({
    required String name,
    required String email,
    required String password,
    required String confirmation,
  }) async {
    try {
      dynamic response = await api.post("user/register", body: {
        "name": name,
        "email": email,
        "password": password,
        "confirmation": confirmation,
      });

      return ResponseBase.fromJson(
          response, RegisterResponse.fromJson(response["data"]));
    } catch (e) {
      rethrow;
    }
  }
}
