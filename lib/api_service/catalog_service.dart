import 'package:client/api_service/api_base.dart';
import 'package:client/constants.dart';
import 'package:client/model/category.dart';
import 'package:client/model/book.dart';
import 'package:client/model/collection.dart';
import 'package:client/model/response_base.dart';

typedef BookMapping = Map<String, List<Book>>;

class CatalogResponse {
  final List<Category> categories;
  final BookMapping books;
  final List<Collection> myItems;

  const CatalogResponse({
    required this.categories,
    required this.books,
    required this.myItems,
  });

  factory CatalogResponse.fromJson(Map<String, dynamic> json) {
    List<Category> categories = [];
    List<Collection> myItems = [];
    dynamic categoriesJson = json["categories"];
    for (int i = 0; i < categoriesJson.length; i++) {
      categories.add(Category.fromJson(categoriesJson[i]));
    }

    dynamic collectionJson = json["my_items"];
    for (int i = 0; i < collectionJson.length; i++) {
      myItems.add(Collection.fromJson(collectionJson[i]));
    }

    BookMapping books = {};
    dynamic booksJson = json["books"];
    booksJson.forEach((key, value) {
      if (!books.containsKey(key)) {
        books[key] = [];
      }
      for (var element in value) {
        books[key]?.add(Book.fromJson(element));
      }
    });
    return CatalogResponse(
        categories: categories, books: books, myItems: myItems);
  }
}

class BookResponse {
  final dynamic book;

  const BookResponse({
    required this.book,
  });

  factory BookResponse.fromJson(List<dynamic> json) {
    dynamic booksJson = json;
    return BookResponse(book: booksJson);
  }
}

class CatalogService {
  final API api = const API(baseUrl: BASE_URL);

  Future<ResponseBase<CatalogResponse>> getHomePageData() async {
    try {
      dynamic response = await api.get("home/");
      return ResponseBase.fromJson(
          response, CatalogResponse.fromJson(response["data"]));
    } catch (e) {
      rethrow;
    }
  }
}

class SearchResponse {
  final List<Book2> books;

  const SearchResponse({
    required this.books,
  });

  factory SearchResponse.fromJson(List<dynamic> json) {
    List<Book2> books = [];
    dynamic booksJson = json;
    for (int i = 0; i < booksJson.length; i++) {
      books.add(Book2.fromJson(booksJson[i]));
    }
    return SearchResponse(books: books);
  }
}

class SearchService {
  final API api = const API(baseUrl: BASE_URL);

  Future<ResponseBase<SearchResponse>> getSearchPageData(String query) async {
    String urlSearch = "book/search/" + query;
    try {
      dynamic response = await api.get(urlSearch);
      return ResponseBase.fromJson(
          response, SearchResponse.fromJson(response["data"]));
    } catch (e) {
      rethrow;
    }
  }

  Future<ResponseBase<SearchResponse>> getFilterPageData(
      List<Category> listquery) async {
    String query = listquery[0].name;
    for (int i = 1; i < listquery.length; i++) {
      query = query + "," + listquery[i].name;
    }
    String urlSearch = "book/category/" + query + ",";
    try {
      dynamic response = await api.get(urlSearch);
      return ResponseBase.fromJson(
          response, SearchResponse.fromJson(response["data"]));
    } catch (e) {
      rethrow;
    }
  }
}

class BookService {
  final API api = const API(baseUrl: BASE_URL);

  Future<ResponseBase> getBooksById(int id) async {
    try {
      dynamic response = await api.get("book/$id");
      return ResponseBase.fromJson(response, response["data"]);
    } catch (e) {
      rethrow;
    }
  }
}
