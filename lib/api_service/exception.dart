class ServiceException implements Exception {
  final dynamic cause;
  ServiceException({required this.cause});
}
