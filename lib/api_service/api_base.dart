import 'dart:convert';

import 'package:client/api_service/exception.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class API {
  final String baseUrl;

  const API({required this.baseUrl});

  Future<Map<String, dynamic>> get(String url,
      [Map<String, String> queryParam = const {}]) async {
    String paramString = "";
    queryParam.forEach((key, value) {
      paramString += "$key=$value";
    });
    final pref = await SharedPreferences.getInstance();
    const key = 'token';
    final readToken = pref.getString(key);
    final headers = <String, String>{
      'Authorization': 'Bearer $readToken',
    };
    if (paramString.isNotEmpty) {
      paramString = "?" + paramString;
    }
    final response = await http.get(Uri.parse("$baseUrl/$url$paramString"),
        headers: headers);

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    }

    throw ServiceException(cause: jsonDecode(response.body));
  }

  Future<Map<String, dynamic>> post(String url,
      {Map<String, dynamic>? body}) async {
    final headers = <String, String>{
      'Content-Type': 'application/json',
    };
    final pref = await SharedPreferences.getInstance();
    const key = 'token';
    final readToken = pref.getString(key);
    headers['Authorization'] = 'Bearer $readToken';
    final response = await http.post(Uri.parse("$baseUrl/$url"),
        body: jsonEncode(body), headers: headers);

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    }
    throw ServiceException(cause: jsonDecode(response.body));
  }

  Future<Map<String, dynamic>> patch(String url,
      {Map<String, dynamic>? body}) async {
    final headers = <String, String>{
      'Content-Type': 'application/json',
    };
    final pref = await SharedPreferences.getInstance();
    const key = 'token';
    final readToken = pref.getString(key);
    headers['Authorization'] = 'Bearer $readToken';

    final response = await http.patch(Uri.parse("$baseUrl/$url"),
        body: jsonEncode(body), headers: headers);

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    }
    throw ServiceException(cause: jsonDecode(response.body));
  }
}
