import 'package:client/api_service/api_base.dart';
import 'package:client/constants.dart';
import 'package:client/model/collection.dart';
import 'package:client/model/response_base.dart';

class MyCollectionResponse {
  final int userId;
  final int bookRead;
  final List<Collection> collections;

  const MyCollectionResponse({
    required this.userId,
    required this.bookRead,
    required this.collections,
  });

  factory MyCollectionResponse.fromJson(Map<String, dynamic> json) {
    List<Collection> collections = [];
    dynamic collectionsJson = json["collections"];

    for (int i = 0; i < collectionsJson.length; i++) {
      collections.add(Collection.fromJson(collectionsJson[i]));
    }

    return MyCollectionResponse(
      userId: json["id_user"],
      bookRead: json["book_read"],
      collections: collections,
    );
  }
}

class CollectionService {
  final API api = const API(baseUrl: BASE_URL);

  Future<void> trackProgress(int id, int progress) async {
    try {
      await api.patch("collection/track/$id/$progress");
    } catch (e) {
      rethrow;
    }
  }

  // ignore: non_constant_identifier_names
  Future<ResponseBase<MyCollectionResponse>> GetCollections() async {
    try {
      dynamic response = await api.get("collection/");
      return ResponseBase.fromJson(
          response, MyCollectionResponse.fromJson(response["data"]));
    } catch (e) {
      rethrow;
    }
  }
}
