import 'package:client/constants.dart';
import 'package:client/routes/routes.dart';
import 'package:client/styles.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  // ignore: use_key_in_widget_constructors
  const MyApp();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ensiklopedi.id',
      color: backgroundColor,
      initialRoute: splashRoute,
      onGenerateRoute: onGenerateRoute,
    );
  }
}
