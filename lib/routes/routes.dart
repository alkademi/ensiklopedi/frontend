import 'package:client/constants.dart';
import 'package:client/screens/home/bottom_navbar.dart';
import 'package:client/screens/home/filter/filter_menu.dart';
import 'package:client/screens/login/login_page.dart';
import 'package:client/screens/bookViewer/pdf_viewer.dart';
import 'package:client/screens/register/register_page.dart';
import 'package:client/screens/splash/Splash.dart';
import 'package:flutter/material.dart';

String initialRoutes() {
  return homeRoute;
}

Route<dynamic> onGenerateRoute(RouteSettings settings) {
  switch (settings.name) {
    case splashRoute:
      {
        return MaterialPageRoute(builder: (context) {
          return const Splash();
        });
      }
    case loginRoute:
      {
        return MaterialPageRoute(builder: (context) {
          return const LoginPage();
        });
      }
    case registerRoute:
      {
        return MaterialPageRoute(builder: (context) {
          return const RegisterPage();
        });
      }
    case homeRoute:
      {
        HomeIndex screenIndex = settings.arguments == null
            ? HomeIndex.katalogIndex
            : settings.arguments as HomeIndex;
        return MaterialPageRoute(builder: (context) {
          return BottomNavbar(
            screenIndex: screenIndex,
          );
        });
      }
    case filterRoute:
      {
        return MaterialPageRoute(builder: (context) {
          return FilterMenu();
        });
      }
    case readRoute:
      {
        return MaterialPageRoute(builder: (context) {
          PDFViewerArgument argument = settings.arguments as PDFViewerArgument;
          return PDFViewer(
            argument: argument,
          );
        });
      }
    default:
      {
        return MaterialPageRoute(builder: (context) {
          return BottomNavbar();
        });
      }
  }
}
