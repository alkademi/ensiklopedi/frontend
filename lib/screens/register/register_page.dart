// ignore: file_names
import 'package:client/header.dart';
import 'package:flutter/material.dart';
import 'input_wrapper_reg.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: ListView(
        children: [
          Container(
            width: double.infinity,
            height: 800,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment(0, -1),
                end: Alignment(
                    0, -0.3), // 10% of the width, so there are ten blinds.
                colors: <Color>[
                  Color.fromARGB(242, 0, 128, 255),
                  Color.fromARGB(255, 229, 229, 229),
                ], // red to yellow
                tileMode:
                    TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
            child: Column(
              children: <Widget>[
                const SizedBox(
                  height: 85,
                ),
                const Header(),
                Expanded(
                    child: Container(
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      )),
                  child: InputWrapperReg(),
                )),
              ],
            ),
          )
        ],
      ),
    );
  }
}
