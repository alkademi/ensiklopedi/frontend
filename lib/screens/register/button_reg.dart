// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  final VoidCallback onPressed;

  const Button({
    Key? key,
    required this.onPressed,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50,
        padding: const EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
          color: const Color.fromARGB(255, 0, 128, 255),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Center(
          child: FlatButton(
            color: Colors.transparent,
            height: 50,
            minWidth: 500,
            onPressed: () {
              onPressed();
            },
            child: const Text(
              "Register",
              style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'Poppins',
                  fontSize: 13,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ));
  }
}
