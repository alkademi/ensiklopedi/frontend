import 'package:client/constants.dart';
import 'package:flutter/material.dart';

class RegisterInputController {
  late bool Function() isValid;
  late Map<String, String> Function() getValues;
}

class InputFieldReg extends StatefulWidget {
  final RegisterInputController controller;

  // ignore: use_key_in_widget_constructors
  const InputFieldReg({required this.controller});
  @override
  // ignore: no_logic_in_create_state
  State<InputFieldReg> createState() => _InputFieldRegState(controller);
}

class _InputFieldRegState extends State<InputFieldReg> {
  _InputFieldRegState(RegisterInputController _controller) {
    _controller.isValid = isValid;
    _controller.getValues = getValues;
  }
  String email = "";
  String name = "";
  String password = "";
  String confirmation = "";
  String passwordErr = "";

  var isPasswordMatch = true;
  var isEmailValid = true;

  void onEmailChange(String newValue) {
    email = newValue;
    setEmailValidator(RegExp(EMAIL_REGEX).hasMatch(email));
  }

  void onNameChange(String newValue) {
    name = newValue;
  }

  void onPasswordChange(String newValue) {
    password = newValue;
    if (password.length < 8) {
      setState(() {
        passwordErr = "Password must be 8 characters long or more";
      });
    } else {
      setState(() {
        passwordErr = "";
      });
    }
  }

  void onConfirmationChange(String newValue) {
    confirmation = newValue;
    setPasswordValidator(confirmation == password);
  }

  void setPasswordValidator(bool valid) {
    setState(() {
      isPasswordMatch = valid;
    });
  }

  void setEmailValidator(bool valid) {
    setState(() {
      isEmailValid = valid;
    });
  }

  bool isValid() {
    return isPasswordMatch &&
        isEmailValid &&
        password.isNotEmpty &&
        confirmation.isNotEmpty &&
        email.isNotEmpty &&
        name.isNotEmpty &&
        password.length >= 8;
  }

  Map<String, String> getValues() {
    return {
      "email": email,
      "name": name,
      "password": password,
      "confirmation": confirmation
    };
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        //Text("Email", textAlign: TextAlign.center, style: TextStyle(fontSize: 13, fontFamily: 'Poppins', fontWeight: FontWeight.w600),),
        const Align(
          alignment: Alignment.centerLeft,
          child: Text("Name",
              style: TextStyle(
                  fontSize: 13,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600)),
        ),

        TextField(
          onChanged: onNameChange,
          decoration: InputDecoration(
              hintText: "John Doe",
              hintStyle: const TextStyle(color: Colors.grey),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: const BorderSide(
                      color: Color.fromARGB(255, 243, 241, 241), width: 0)),
              fillColor: const Color.fromARGB(255, 243, 241, 241),
              filled: true),
        ),
        const SizedBox(
          height: 8,
        ),
        const Align(
          alignment: Alignment.centerLeft,
          child: Text("Email",
              style: TextStyle(
                  fontSize: 13,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600)),
        ),

        TextField(
          onChanged: onEmailChange,
          decoration: InputDecoration(
              errorText: isEmailValid ? null : "invalid email",
              hintText: "example@gmail.com",
              hintStyle: const TextStyle(
                  color: Color.fromARGB(255, 158, 158, 158), fontSize: 13),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: const BorderSide(
                      color: Color.fromARGB(255, 243, 241, 241), width: 0)),
              fillColor: const Color.fromARGB(255, 243, 241, 241),
              filled: true),
        ),
        const SizedBox(
          height: 8,
        ),
        const Align(
          alignment: Alignment.centerLeft,
          child: Text("Password",
              style: TextStyle(
                  fontSize: 13,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600)),
        ),
        TextField(
          onChanged: onPasswordChange,
          obscureText: true,
          obscuringCharacter: "*",
          decoration: InputDecoration(
              hintText: "........",
              hintStyle: const TextStyle(color: Colors.grey),
              errorText: passwordErr.isNotEmpty ? passwordErr : null,
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: const BorderSide(
                      color: Color.fromARGB(255, 243, 241, 241), width: 0)),
              fillColor: const Color.fromARGB(255, 243, 241, 241),
              filled: true),
        ),
        const SizedBox(
          height: 8,
        ),
        const Align(
          alignment: Alignment.centerLeft,
          child: Text("Confirm Password",
              style: TextStyle(
                  fontSize: 13,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600)),
        ),

        TextField(
          onChanged: onConfirmationChange,
          obscureText: true,
          obscuringCharacter: "*",
          decoration: InputDecoration(
              errorText: isPasswordMatch ? null : "Password not match",
              hintText: "........",
              hintStyle: const TextStyle(color: Colors.grey),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: const BorderSide(
                      color: Color.fromARGB(255, 243, 241, 241), width: 0)),
              fillColor: const Color.fromARGB(255, 243, 241, 241),
              filled: true),
        ),
      ],
    );
  }
}
