// ignore: file_names
import 'package:client/api_service/exception.dart';
import 'package:client/api_service/user_service.dart';
import 'package:client/constants.dart';
import 'package:client/screens/login/login_page.dart';
import 'package:flutter/material.dart';

import 'button_reg.dart';
import 'input_field_reg.dart';

class InputWrapperReg extends StatelessWidget {
  final RegisterInputController controller = RegisterInputController();
  final UserService userService = UserService();

  InputWrapperReg({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    register() async {
      try {
        if (controller.isValid()) {
          var body = controller.getValues();
          await userService.register(
              name: body["name"] ?? "",
              email: body["email"] ?? "",
              password: body["password"] ?? "",
              confirmation: body["confirmation"] ?? "");
          showDialog(
              context: context,
              builder: (BuildContext ctx) {
                return AlertDialog(
                  title: const Text("Success"),
                  content: const Text("You have successfully registered!"),
                  actions: <Widget>[
                    TextButton(
                      child: const Text("OK"),
                      onPressed: () {
                        Navigator.of(ctx).pop();
                        Navigator.of(context).pushReplacement(
                            MaterialPageRoute(builder: (BuildContext ctx) {
                          return const LoginPage();
                        }));
                      },
                    )
                  ],
                );
              });
        }
      } on ServiceException catch (e) {
        String message = e.cause["message"] ?? "";
        showDialog(
            context: context,
            builder: (BuildContext ctx) => AlertDialog(
                  title: const Text("Error"),
                  content: Text(message),
                  actions: <Widget>[
                    TextButton(
                        onPressed: () => Navigator.pop(ctx),
                        child: const Text("Ok"))
                  ],
                ));
      }
    }

    return Container(
      margin: const EdgeInsets.only(top: 10.0),
      padding: const EdgeInsets.only(top: 10, right: 30, left: 30),
      child: Column(
        children: [
          const Icon(
            Icons.account_circle_outlined,
            color: Colors.blue,
            size: 90.0,
          ),
          Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(10)),
            child: InputFieldReg(
              controller: controller,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Button(
            onPressed: () {
              register();
            },
          ),
          const SizedBox(
            height: 20,
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            const Text(
              "Already have an account?",
              style: TextStyle(color: Colors.grey),
            ),
            InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const LoginPage()));
                },
                child: Container(
                  margin: const EdgeInsets.only(left: 10),
                  child: GestureDetector(
                    child: const Text(
                      "Login",
                      style: TextStyle(color: Colors.blue),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, loginRoute);
                    },
                  ),
                ))
          ])
        ],
      ),
    );
  }
}
