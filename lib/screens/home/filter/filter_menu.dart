import 'package:flutter/material.dart';

// ignore: must_be_immutable
class FilterMenu extends StatelessWidget {
  var filterList = [
    "all",
    "comic",
    "marvel",
    "children",
    "education",
    "art",
    "coding",
    "phsycology",
    "game",
    "fun",
    "cooking"
  ];

  FilterMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: <Color>[
              Color.fromARGB(242, 0, 128, 255),
              Color.fromARGB(110, 0, 128, 255),
            ], // red to yellow
            tileMode: TileMode.repeated, // repeats the gradient over the canvas
          ),
        ),
        child: Column(
          children: [
            const SizedBox(
              height: 80,
            ),
            Container(
              alignment: Alignment.centerLeft,
              padding:
                  const EdgeInsets.symmetric(horizontal: 50.0, vertical: 20),
              child: Image.asset(
                'assets/icon/left-arrow-2.png',
                width: 30,
                height: 30,
                color: Colors.white,
              ),
            ),
            Container(
              // color: Colors.white,
              margin: const EdgeInsets.symmetric(vertical: 20),
              alignment: Alignment.centerLeft,
              child: const Padding(
                padding: EdgeInsets.symmetric(horizontal: 50.0),
                child: Text(
                  'Filter',
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Poppins',
                      fontSize: 48,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 0, horizontal: 50),
              // color: Colors.white,
              alignment: Alignment.topLeft,
              height: 400,
              child: GridView(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: 10,
                    childAspectRatio: 2.3,
                    mainAxisSpacing: 25),
                children: filterList.map((e) {
                  return CategoriItem(e);
                }).toList(),
              ),
            ),
            Container(
              height: 40,
              width: 150,
              decoration: const BoxDecoration(
                  color: Color.fromARGB(255, 255, 255, 255),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                    bottomRight: Radius.circular(30),
                    bottomLeft: Radius.circular(30),
                  )),
              child: TextButton(
                onPressed: () {},
                // style: ButtonStyle(
                //     backgroundColor: MaterialStateProperty.all<Color>(
                //         Color.fromARGB(1, 255, 255, 255))),
                child: const Text(
                  "Confirm",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class CategoriItem extends StatelessWidget {
  final String name;
  const CategoriItem(this.name, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          color: Color.fromARGB(255, 118, 171, 223),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30),
            topRight: Radius.circular(30),
            bottomRight: Radius.circular(30),
            bottomLeft: Radius.circular(30),
          )),
      child: TextButton(
        onPressed: () {},
        // style: ButtonStyle(
        //     backgroundColor: MaterialStateProperty.all<Color>(
        //         Color.fromARGB(1, 255, 255, 255))),
        child: Text(
          name,
          style: const TextStyle(
            color: Colors.white,
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}
