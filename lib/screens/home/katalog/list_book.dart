import 'package:client/constants.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import '../../bookViewer/pdf_viewer.dart';

class MyBooksList extends StatefulWidget {
  const MyBooksList({
    Key? key,
    required this.image,
    required this.title,
    required this.progress,
    required this.content,
    required this.price,
    required this.rating,
    required this.category,
    required this.desc,
    required this.buy,
    required this.id,
    required this.author,
  }) : super(key: key);

  final String image;
  final String title;
  final int price;
  final double rating;
  final String category;
  final String content;
  final bool buy;
  final String author;
  final String desc;
  final int id;
  final int progress;

  @override
  State<MyBooksList> createState() => _MyBooksListState();
}

class _MyBooksListState extends State<MyBooksList> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => {
        Navigator.pushNamed(context, readRoute,
            arguments: PDFViewerArgument(
                content: widget.content,
                title: widget.title,
                bookId: widget.id,
                progress: widget.progress))
      },
      child: Container(
        margin: const EdgeInsets.only(top: 20, bottom: 10, right: 10),
        height: 200,
        width: 140,
        padding:
            const EdgeInsets.only(top: 10, left: 15, right: 15, bottom: 10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(style: BorderStyle.none),
            boxShadow: const [
              BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 1),
            ]),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                margin: const EdgeInsets.only(left: 4),
                child: LinearPercentIndicator(
                  width: 100,
                  animation: true,
                  animationDuration: 1000,
                  lineHeight: 8.0,
                  barRadius: const Radius.circular(8),
                  percent: widget.progress.toDouble() / 100.0,
                  backgroundColor: const Color.fromARGB(255, 221, 218, 218),
                  progressColor: Colors.blue,
                ),
              ),
              Image.network("$BASE_URL/file/${widget.image}", width: 80),
              const SizedBox(
                height: 8,
              ),
              Flexible(
                  child: Text(
                widget.title,
                textAlign: TextAlign.center,
                maxLines: 2,
                style: const TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 12,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              )),
            ]),
      ),
    );
  }
}
