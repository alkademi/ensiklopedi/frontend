// ignore_for_file: non_constant_identifier_names

import 'dart:math';

import 'package:client/api_service/catalog_service.dart';
import 'package:client/components/screens/resultSearch/result_search.dart';
import 'package:client/constants.dart';
import 'package:client/model/category.dart';
import 'package:client/model/book.dart';
import 'package:client/model/collection.dart';
import 'package:client/screens/home/bottom_navbar.dart';
import 'package:client/styles.dart';
import 'package:flutter/material.dart';
import '../../../api_service/collection_service.dart';
import '../../../model/collection.dart';
import 'list_book.dart';
import 'list_book_categories.dart';

class Katalog extends StatefulWidget {
  const Katalog({Key? key}) : super(key: key);

  @override
  _KatalogState createState() => _KatalogState();
}

class _KatalogState extends State<Katalog> {
  List<Category> jsonCategory = [];
  BookMapping jsonBook = {};
  var rng = Random();

  CollectionService collectionService = CollectionService();
  List<Collection> collections = [];

  List<String> jsonCategoryAll = ['All'];
  List<String> jsonCategory2 = [];
  var jsonCategoryId = [];
  var jsonBookPerCategory = [];
  var bookPerCategory = [];
  var bookId = [];
  var jsonBookTitle_temp = [];
  var jsonOwned_temp = [];
  String querySearch = '';
  List<Collection> myItems = [];

  final CatalogService catalogService = CatalogService();

  Future<void> initData() async {
    final homeData = (await catalogService.getHomePageData()).data;
    myItems = homeData.myItems;
    jsonCategory = homeData.categories;
    jsonBook = homeData.books;

    // print(jsonBook);
    // print(jsonCategory);
    // print(myItems);

    for (int i = 0; i < jsonBook.length; i++) {
      jsonBookPerCategory.add(jsonBook[jsonCategory[i].name]);
    }

    for (var element in jsonCategory) {
      jsonCategory2.add(element.name);
      jsonCategoryAll.add(element.name);
      jsonCategoryId.add(element.id);
    }

    MyCollectionResponse response =
        (await collectionService.GetCollections()).data;

    setState(() {
      jsonCategory = jsonCategory;
      jsonBook = jsonBook;
      jsonCategory2 = jsonCategory2;
      jsonCategoryAll = jsonCategoryAll;
      jsonCategoryId = jsonCategoryId;
      jsonBookPerCategory = jsonBookPerCategory;
      collections = response.collections;
    });
  }

  @override
  void initState() {
    super.initState();
    initData();
  }

  int _isOn = 0;
  int idx = 0;

  @override
  Widget build(BuildContext context) {
    //HEADER
    Widget header() {
      return Container(
          alignment: AlignmentDirectional.topStart,
          child: Row(
            children: [
              Text(
                'Ensiklopedi.id',
                style: titleText,
              ),
              const Spacer(),
            ],
          ));
    }

    void changeQuery(String value) {
      setState(() {
        querySearch = value;
      });
    }

    //FITUR SEARCH
    Widget searchField() {
      return Container(
          margin: const EdgeInsets.only(right: 0),
          height: 50,
          child: TextField(
            onChanged: (value) => {changeQuery(value)},
            decoration: InputDecoration(
              hintText: 'Find your favorite books',
              fillColor: searchBackground,
              filled: true,
              hintStyle: placeholderText,
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15),
                  borderSide: BorderSide.none),
              suffixIcon: InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ResultSearch(
                                query: querySearch,
                              )));
                },
                child: Container(
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: blueDark,
                        borderRadius: BorderRadius.circular(15)),
                    child:
                        const Icon(Icons.search_rounded, color: Colors.white)),
              ),
            ),
          ));
    }

    //FITUR MYBOOKS
    Widget MyBooks(int index) {
      var categoryMyBookInMyItems = [];
      var priceMyBookInMyItems = [];
      var ratingMyBookInMyItems = [];
      var buyMyBookInMyItems = [];
      var authorMyBookInMyItems = [];
      var descMyBookInMyItems = [];
      var contentMyBookInMyItems = [];
      var imageMyBookInMyItems = [];
      var percentMyBookInMyItems = [];
      var titleMyBookInMyItems = [];
      var idMyBookInMyItems = [];

      for (int i = 0; i < jsonBookPerCategory.length; i++) {
        for (int j = 0; j < jsonBookPerCategory[i].length; j++) {
          for (int k = 0; k < myItems.length; k++) {
            if (jsonBookPerCategory[i][j].id == myItems[k].idBook) {
              categoryMyBookInMyItems.add(jsonCategory2[i]);
              priceMyBookInMyItems.add(jsonBookPerCategory[i][j].price);
              imageMyBookInMyItems.add(jsonBookPerCategory[i][j].cover);
              idMyBookInMyItems.add(jsonBookPerCategory[i][j].id);
              percentMyBookInMyItems.add(jsonBookPerCategory[i][j].progress);
              titleMyBookInMyItems.add(jsonBookPerCategory[i][j].title);
              ratingMyBookInMyItems.add(4.5);
              buyMyBookInMyItems.add(jsonBookPerCategory[i][j].isOwned);
              authorMyBookInMyItems.add(jsonBookPerCategory[i][j].author);
              descMyBookInMyItems.add(jsonBookPerCategory[i][j].description);
              contentMyBookInMyItems.add(jsonBookPerCategory[i][j].content);
            }
          }
        }
      }

      return SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: InkWell(
            onTap: () {
              setState(() {
                _isOn = index + 1;
              });
            },
            child: Container(
                margin: const EdgeInsets.only(top: 0, right: 20),
                child: Row(
                  children: [
                    for (int i = 0; i < myItems.length; i++)
                      MyBooksList(
                        image: imageMyBookInMyItems[i],
                        title: titleMyBookInMyItems[i],
                        progress: percentMyBookInMyItems[i],
                        content: contentMyBookInMyItems[i],
                        price: priceMyBookInMyItems[i],
                        rating: ratingMyBookInMyItems[i],
                        category: categoryMyBookInMyItems[i],
                        buy: buyMyBookInMyItems[i],
                        id: idMyBookInMyItems[i],
                        author: authorMyBookInMyItems[i],
                        desc: descMyBookInMyItems[i],
                      )
                  ],
                )),
          ));
    }

    // CATEGORY
    Widget Categories(int index) {
      return InkWell(
        onTap: () {
          setState(() {
            _isOn = index;
          });
        },
        child: FittedBox(
          child: Container(
              margin: const EdgeInsets.only(top: 15, bottom: 15, right: 10),
              padding: EdgeInsets.symmetric(
                  horizontal: index == 0 ? 30 : 25, vertical: 10),
              decoration: BoxDecoration(
                color: _isOn == index ? blueDark : blueLight,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Text(
                jsonCategoryAll[index],
                style: categoryText,
                textAlign: TextAlign.center,
              )),
        ),
      );
    }

    //MENYIMPAN KATEGORI-KATEGORI KE LIST
    Widget ListCategories() {
      return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: jsonCategoryAll
              .asMap()
              .entries
              .map((MapEntry map) => Categories(map.key))
              .toList(),
        ),
      );
    }

    //"GENRE  SEEMORE"
    Widget AllCategoriesList(int index) {
      return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Container(
            alignment: AlignmentDirectional.topStart,
            margin: EdgeInsets.only(top: index == 0 ? 5 : 30),
            child: Text(
              jsonCategory2[index],
              style: subTitleText,
              textAlign: TextAlign.center,
            )),
        InkWell(
            onTap: () {
              setState(() {
                _isOn = index + 1;
              });
            },
            child: Container(
                margin: EdgeInsets.only(top: index == 0 ? 0 : 20),
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                width: 72,
                decoration: BoxDecoration(
                  color: blueLight,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Text(
                  'See More',
                  style: seeMoreText,
                  textAlign: TextAlign.center,
                )))
      ]);
    }

    //JUST GENRE
    Widget AllCategoriesListGenre(int index) {
      return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Container(
            alignment: AlignmentDirectional.topStart,
            margin: EdgeInsets.only(top: index == 0 ? 5 : 15),
            padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 0),
            child: Text(
              jsonCategory2[index],
              style: subTitleText,
              textAlign: TextAlign.center,
            ))
      ]);
    }

    //MENYIMPAN DATA BUKU SESUAI KATEGORI
    Widget BookListCategory(String category, int index) {
      List<Book> bookCategories = jsonBook[category] ?? [];
      List<bool> buy2 = [true, false];
      buy2.shuffle();
      bookCategories.shuffle();

      var bookTitle = [];
      var bookOwned = [];

      for (int i = 0; i < jsonBookPerCategory.length; i++) {
        if (i == index) {
          for (int j = 0; j < jsonBookPerCategory[i].length; j++) {
            bookId.add(jsonBookPerCategory[i][j].id);
            bookTitle.add(jsonBookPerCategory[i][j].title);
            bookOwned.add(jsonBookPerCategory[i][j].isOwned);
          }
        }
      }
      return Container(
        margin: EdgeInsets.only(top: index == 0 ? 10 : 0, left: 10, right: 0),
        child: Column(children: [
          _isOn == 0 ? AllCategoriesList(index) : AllCategoriesListGenre(index),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.only(top: 20),
            child: Wrap(spacing: 10, children: [
              for (int i = 0; i < bookCategories.length; i++)
                BookList(
                  title: bookTitle[i],
                  image: bookCategories[i].cover,
                  price: bookCategories[i].price,
                  rating: 4.5,
                  desc: bookCategories[i].description,
                  author: bookCategories[i].author,
                  category: category,
                  content: bookCategories[i].content,
                  buy: bookOwned[i],
                  id: bookCategories[i].id,
                )
            ]),
          ),
        ]),
      );
    }

    return Scaffold(
        backgroundColor: backgroundColor,
        body: RefreshIndicator(
          onRefresh: (() {
            return initData();
          }),
          child: ListView(
            children: [
              Container(
                  padding:
                      const EdgeInsets.symmetric(vertical: 25, horizontal: 25),
                  decoration: BoxDecoration(color: backgroundColor),
                  child: Column(
                    children: [
                      header(),
                      const SizedBox(height: 20),
                      searchField(),
                      const SizedBox(height: 5),
                    ],
                  )),
              Container(
                  padding:
                      const EdgeInsets.symmetric(vertical: 25, horizontal: 25),
                  decoration: BoxDecoration(
                    color: backgroundColor,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25)),
                    boxShadow: const [
                      BoxShadow(
                          color: Colors.black38,
                          spreadRadius: 0,
                          blurRadius: 1),
                    ],
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('My Books',
                                style: titleText.copyWith(color: textHeading)),
                            InkWell(
                              onTap: () {
                                Navigator.pushNamed(context, homeRoute,
                                    arguments: HomeIndex.bookmarkIndex);
                              },
                              child: Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 8),
                                  width: 72,
                                  decoration: BoxDecoration(
                                    color: blueLight,
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  child: Text(
                                    'See More',
                                    style: seeMoreText,
                                    textAlign: TextAlign.center,
                                  )),
                            )
                          ]),
                      myItems.isEmpty
                          ? Container(
                              margin:
                                  const EdgeInsets.only(top: 30, bottom: 30),
                              child: Center(
                                  child: Text("There is no book",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: textSubHeading))))
                          : MyBooks(_isOn),
                      ListCategories(),
                      for (int index = 0;
                          _isOn == 0 ? index < jsonCategory2.length : index < 1;
                          index++)
                        _isOn == 0
                            ? BookListCategory(jsonCategory2[index], index)
                            : BookListCategory(
                                jsonCategory2[_isOn - 1], _isOn - 1),
                    ],
                  )),
            ],
          ),
        ));
  }
}
