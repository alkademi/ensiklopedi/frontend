import 'package:client/components/detail/detail_book.dart';
import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../../styles.dart';

class BookList extends StatefulWidget {
  const BookList(
      {Key? key,
      required this.image,
      required this.title,
      required this.price,
      required this.rating,
      required this.category,
      required this.content,
      required this.desc,
      required this.buy,
      required this.id,
      required this.author})
      : super(key: key);

  final String image;
  final String title;
  final int price;
  final String desc;
  final double rating;
  final String content;
  final String category;
  final bool buy;
  final String author;
  final int id;

  @override
  State<BookList> createState() => _BookListState();
}

class _BookListState extends State<BookList> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailBook(
                      index: widget.id,
                    )))
      },
      child: Container(
        margin: const EdgeInsets.only(top: 12, bottom: 2),
        height: 220,
        width: 140,
        padding: const EdgeInsets.only(top: 0, left: 2, right: 2, bottom: 0),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(style: BorderStyle.none),
            boxShadow: const [
              BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 1),
            ]),
        child:
            Column(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Image.network(
              "$BASE_URL/file/${widget.image}",
              width: 80,
              height: 120,
            ),
          ),
          Container(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: Text(
              widget.title,
              style: bookTitle.copyWith(fontSize: 11),
              maxLines: 2,
              textAlign: TextAlign.center,
            ),
          ),
          widget.buy == false
              ? Text(
                  widget.price == 0
                      ? 'Free'
                      : 'Rp' + widget.price.toString() + '.-',
                  style: bookTitle.copyWith(fontSize: 13))
              : SizedBox(
                  child: Text(
                    "Owned",
                    style:
                        TextStyle(color: blueDark, fontWeight: FontWeight.w800),
                  ),
                )
        ]),
      ),
    );
  }
}
