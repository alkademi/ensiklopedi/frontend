import 'package:client/api_service/collection_service.dart';
import 'package:client/constants.dart';
import 'package:client/model/collection.dart';
import 'package:client/styles.dart';
import 'package:flutter/material.dart';

import '../../bookViewer/pdf_viewer.dart';

class Bookmark extends StatefulWidget {
  const Bookmark({Key? key}) : super(key: key);

  @override
  State<Bookmark> createState() => _BookmarkState();
}

class _BookmarkState extends State<Bookmark> {
  CollectionService collectionService = CollectionService();
  List<Collection> collections = [];
  int bookReads = 0;

  Widget _buildHeader() {
    return Row(
      children: [
        Expanded(
          child: Image.asset(
            "assets/image/man-read.png",
          ),
        ),
        Expanded(
            child: Text(
          "$bookReads Books\nhave been read",
          style: const TextStyle(fontWeight: FontWeight.w700, fontSize: 24),
        ))
      ],
    );
  }

  Widget bookTile(int index) {
    Collection collection = collections[index];

    double progress = collection.progress.toDouble() / 100.0;
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, readRoute,
            arguments: PDFViewerArgument(
                content: collection.book.content,
                title: collection.book.title,
                bookId: collection.book.id,
                progress: collection.progress));
      },
      child: Container(
        padding: const EdgeInsets.only(bottom: 8),
        child: Row(
          // crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ClipRRect(
                child: Image.network(
                  "$BASE_URL/file/${collection.book.cover}",
                  width: 100,
                ),
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    collection.book.title,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Text(
                    collection.book.author,
                    style: const TextStyle(
                        color: Color.fromARGB(128, 0, 0, 0),
                        fontWeight: FontWeight.w500),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  SizedBox(
                    width: 300,
                    child: Row(
                      children: [
                        SizedBox(
                          height: 10,
                          width: 150,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: LinearProgressIndicator(
                              value: progress,
                              color: const Color(0xFF007FFF),
                              backgroundColor: const Color(0xFFF3F1F1),
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Text("${(progress * 100).toInt()}%")
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildCollectionList() {
    var collectionList = SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: collections.length,
            itemBuilder: (context, index) {
              return bookTile(index);
            }));

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const [
            Text(
              "Your books",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            )
          ],
        ),
        Expanded(
          child: collectionList,
        )
      ],
    );
  }

  void initData() async {
    MyCollectionResponse response =
        (await collectionService.GetCollections()).data;

    setState(() {
      collections = response.collections;
      bookReads = response.bookRead;
    });
  }

  @override
  void initState() {
    super.initState();
    initData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: ListView(
          children: [
            Container(
              width: double.infinity,
              height: 800,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color.fromARGB(242, 0, 128, 255),
                    Color.fromARGB(255, 229, 229, 229)
                  ],
                  begin: Alignment(0, -1),
                  end: Alignment(0, -0.3),
                ),
              ),
              child: Column(
                children: [
                  const Padding(
                    padding: EdgeInsets.fromLTRB(20, 50, 50, 30),
                    child: Text(
                      "My Book \n Collection",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 33,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'Poppins'),
                    ),
                  ),
                  Expanded(
                      child: Container(
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30),
                        )),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 30, 20, 0),
                      child: Column(
                        children: [
                          _buildHeader(),
                          const SizedBox(
                            height: 10,
                          ),
                          Expanded(
                              child: collections.isEmpty
                                  ? Container(
                                      alignment: Alignment.center,
                                      child: Text("No book that you have!",
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.w700,
                                              color: textSubHeading)))
                                  : _buildCollectionList())
                        ],
                      ),
                    ),
                  )),
                ],
                crossAxisAlignment: CrossAxisAlignment.stretch,
              ),
            )
          ],
        ));
  }
}
