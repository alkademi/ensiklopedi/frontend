import 'package:client/screens/home/bookmark/bookmark_page.dart';
import 'package:client/screens/home/katalog/katalog_page.dart';
import 'package:client/screens/home/profile/profile_page.dart';
import 'package:flutter/material.dart';

enum HomeIndex { katalogIndex, bookmarkIndex, profileIndex }

// ignore: must_be_immutable
class BottomNavbar extends StatefulWidget {
  HomeIndex screenIndex = HomeIndex.katalogIndex;
  BottomNavbar({Key? key, this.screenIndex = HomeIndex.katalogIndex})
      : super(key: key);

  @override
  _BottonNavbarState createState() => _BottonNavbarState();
}

class _BottonNavbarState extends State<BottomNavbar> {
  int _index = 0;
  final List<Widget> _screens = [
    const Katalog(),
    const Bookmark(),
    const Profile()
  ];

  @override
  void initState() {
    super.initState();
    switch (widget.screenIndex) {
      case HomeIndex.bookmarkIndex:
        {
          _index = 1;
          break;
        }
      case HomeIndex.katalogIndex:
        {
          _index = 0;
          break;
        }
      case HomeIndex.profileIndex:
        {
          _index = 2;
          break;
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget customBottomNav() {
      return Container(
          height: 60,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(30), topLeft: Radius.circular(30)),
            boxShadow: [
              BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 4),
            ],
          ),
          child: ClipRRect(
              borderRadius: const BorderRadius.only(
                  topRight: Radius.circular(30), topLeft: Radius.circular(30)),
              child: BottomNavigationBar(
                type: BottomNavigationBarType.fixed,
                currentIndex: _index,
                onTap: (i) {
                  setState(() {
                    _index = i;
                  });
                },
                items: [
                  BottomNavigationBarItem(
                    icon: Container(
                      margin: const EdgeInsets.only(top: 10),
                      child: _index == 0
                          ? Image.asset('assets/icon/home2.png')
                          : Image.asset('assets/icon/home.png'),
                      width: 20,
                    ),
                    label: '',
                  ),
                  BottomNavigationBarItem(
                    icon: Container(
                      margin: const EdgeInsets.only(top: 10),
                      child: _index == 1
                          ? Image.asset('assets/icon/Library2.png')
                          : Image.asset('assets/icon/Library.png'),
                      width: 20,
                    ),
                    label: '',
                  ),
                  BottomNavigationBarItem(
                    icon: Container(
                      margin: const EdgeInsets.only(top: 10),
                      child: _index == 2
                          ? Image.asset('assets/icon/profile2.png')
                          : Image.asset('assets/icon/profile.png'),
                      width: 20,
                    ),
                    label: '',
                  ),
                ],
              )));
    }

    return Scaffold(
      bottomNavigationBar: customBottomNav(),
      body: Stack(
        children: _screens
            .asMap()
            .map(((i, screen) =>
                MapEntry(i, Offstage(offstage: _index != i, child: screen))))
            .values
            .toList(),
      ),
    );
  }
}
