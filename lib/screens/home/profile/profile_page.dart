// ignore_for_file: deprecated_member_use

import 'package:client/api_service/user_service.dart';
import 'package:client/screens/login/login_page.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../styles.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  var name = "";
  var email = "";
  int balance = 0;

  final UserService userService = UserService();

  void initData() async {
    final userData = (await userService.fetchUser()).data;

    setState(() {
      name = userData.name;
      email = userData.email;
      balance = userData.balance;
    });
  }

  @override
  void initState() {
    super.initState();
    initData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Container(
            height: 650,
            margin: const EdgeInsets.only(top: 60),
            padding: const EdgeInsets.only(left: 5, right: 10),
            child: Column(children: [
              Container(
                margin: const EdgeInsets.only(bottom: 20, left: 30),
                alignment: Alignment.topLeft,
                child: const Text(
                  "Your Profile",
                  style: TextStyle(
                    fontSize: 42,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF399BFD),
                    fontFamily: "Poppins",
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 20, bottom: 40),
                child: Image.asset(
                  "assets/icon/yourprofile.png",
                  width: 150,
                  height: 150,
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                margin: const EdgeInsets.only(left: 30, bottom: 10),
                child: const Text(
                  "Name",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontFamily: "Poppins",
                  ),
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                margin: const EdgeInsets.only(left: 30, bottom: 5),
                child: Text(
                  name,
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.normal,
                    color: Colors.black,
                    fontFamily: "Poppins",
                  ),
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                margin: const EdgeInsets.only(top: 20, left: 30, bottom: 10),
                child: const Text(
                  "Email",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontFamily: "Poppins",
                  ),
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                margin: const EdgeInsets.only(left: 30, bottom: 5),
                child: Text(
                  email,
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.normal,
                    color: Colors.black,
                    fontFamily: "Poppins",
                  ),
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                margin: const EdgeInsets.only(top: 20, left: 30, bottom: 10),
                child: const Text(
                  "Balance",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontFamily: "Poppins",
                  ),
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                margin: const EdgeInsets.only(left: 30, bottom: 5),
                child: Text(
                  "Rp" + balance.toString() + ",-",
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.normal,
                    color: Colors.black,
                    fontFamily: "Poppins",
                  ),
                ),
              ),
              Container(
                  padding: const EdgeInsets.only(top: 20),
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      onPressed: () async {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const LoginPage()),
                        );
                        SharedPreferences preferences =
                            await SharedPreferences.getInstance();
                        await preferences.clear();
                      },
                      color: blueLight,
                      textColor: Colors.white,
                      child: const Text('Logout')))
            ]),
          )
        ],
      ),
    );
  }
}
