// ignore_for_file: must_be_immutable

import 'package:client/api_service/collection_service.dart';
import 'package:client/components/detail/detail_book.dart';
import 'package:client/screens/home/bottom_navbar.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

import '../../constants.dart';
import '../../model/collection.dart';

class PDFViewerArgument {
  final String content;
  final String title;
  final int bookId;
  final int progress;
  PDFViewerArgument(
      {required this.content,
      required this.title,
      required this.bookId,
      required this.progress});
}

class PDFViewer extends StatefulWidget {
  PDFViewerArgument argument;
  PDFViewer({Key? key, required this.argument}) : super(key: key);

  @override
  _PDFViewer createState() => _PDFViewer();
}

class _PDFViewer extends State<PDFViewer> {
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();
  late PdfViewerController _pdfViewerController;
  final CollectionService collectionService = CollectionService();

  List<Collection> collections = [];

  PDFViewerArgument get argument => widget.argument;

  void initData() async {
    MyCollectionResponse response =
        (await collectionService.GetCollections()).data;

    setState(() {
      collections = response.collections;
    });
  }

  void goToLastPage(PdfDocumentLoadedDetails details) {
    _pdfViewerController.jumpToPage(
        (argument.progress * _pdfViewerController.pageCount) ~/ 100);
  }

  @override
  void initState() {
    _pdfViewerController = PdfViewerController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(argument.title),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () async {
            int progress = _pdfViewerController.pageNumber *
                100 ~/
                _pdfViewerController.pageCount;
            await collectionService.trackProgress(argument.bookId, progress);
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => BottomNavbar()));
          },
        ),
      ),
      body: SfPdfViewer.network(
        "$BASE_URL/file/${argument.content}",
        key: _pdfViewerKey,
        controller: _pdfViewerController,
        onDocumentLoaded: goToLastPage,
      ),
    );
  }
}
