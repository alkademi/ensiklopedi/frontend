// ignore_for_file: deprecated_member_use

import 'package:client/api_service/exception.dart';
import 'package:client/api_service/payment_service.dart';
import 'package:client/components/detail/detail_book.dart';
import 'package:client/constants.dart';
import 'package:client/screens/home/bottom_navbar.dart';
import 'package:client/styles.dart';
import 'package:flutter/material.dart';

class PaymentPage extends StatefulWidget {
  const PaymentPage(
      {Key? key,
      required this.index,
      required this.buy,
      required this.category,
      required this.title,
      required this.image,
      required this.price,
      required this.rating,
      required this.author})
      : super(key: key);

  final int index;
  final bool buy;
  final String category;
  final String title;
  final String image;
  final int price;
  final double rating;
  final String author;

  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<PaymentPage> {
  final PaymentService paymentService = PaymentService();
  int get index => widget.index;
  bool get buy => widget.buy;
  String get category => widget.category;
  String get title => widget.title;
  String get image => widget.image;
  int get price => widget.price;
  double get rating => widget.rating;
  String get author => widget.author;

  String phoneNumber = '';
  String? selectedValue;
  final _dropdownFormKey = GlobalKey<FormState>();

  void onChangePhone(String newValue) {
    setState(() {
      phoneNumber = newValue;
    });
  }

  List<DropdownMenuItem<String>> get dropdownItems {
    List<DropdownMenuItem<String>> menuItems = [
      const DropdownMenuItem(child: Text("Balance"), value: "BALANCE"),
      const DropdownMenuItem(child: Text("Ovo"), value: "ID_OVO"),
    ];
    return menuItems;
  }

  @override
  Widget build(BuildContext context) {
    buyBook() async {
      try {
        final response = await paymentService.buy(
            book_id: index,
            payment_method: selectedValue.toString(),
            phoneNumber: phoneNumber);
        if (response.data.status.isNotEmpty) {
          showDialog(
              context: context,
              builder: (BuildContext ctx) => AlertDialog(
                    title: const Text("Status Pembelian"),
                    content: response.data.metadata.payment_method == "ID_OVO"
                        ? const Text(
                            "Please complete your payment on your OVO app!")
                        : const Text("Purchase was successful!"),
                    actions: <Widget>[
                      TextButton(
                          onPressed: () => {
                                if (_dropdownFormKey.currentState!.validate())
                                  {
                                    //valid flow
                                  },
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => BottomNavbar()))
                              },
                          child: const Text("Home"))
                    ],
                  ));
        }
      } on ServiceException catch (e) {
        String message = e.cause["message"] ?? "";
        showDialog(
            context: context,
            builder: (BuildContext ctx) => AlertDialog(
                  title: const Text("Error"),
                  content: Text(message),
                  actions: <Widget>[
                    TextButton(
                        onPressed: () => Navigator.pop(ctx),
                        child: const Text("Ok"))
                  ],
                ));
      }
    }

    Widget dropDownMenu() {
      return Container(
        padding: const EdgeInsets.symmetric(horizontal: 40),
        margin: const EdgeInsets.only(bottom: 30),
        child: Form(
            key: _dropdownFormKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                DropdownButtonFormField(
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide:
                              const BorderSide(color: Colors.black, width: 1),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        border: OutlineInputBorder(
                          borderSide:
                              const BorderSide(color: Colors.blue, width: 2),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        contentPadding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 10),
                        fillColor: const Color.fromARGB(160, 175, 203, 252),
                        hintText: "Select a payment method"),
                    validator: (value) =>
                        value == null ? "Select a payment method" : null,
                    dropdownColor: const Color.fromARGB(255, 253, 253, 253),
                    value: selectedValue,
                    onChanged: (String? newValue) {
                      setState(() {
                        selectedValue = newValue!;
                      });
                    },
                    items: dropdownItems),
              ],
            )),
      );
    }

    Widget payment() {
      return ListView(children: [
        Container(
            alignment: Alignment.center,
            child: Column(children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.network("$BASE_URL/file/$image", width: 100),
              ),
              Container(
                margin: const EdgeInsets.only(top: 20),
                width: 250,
                alignment: Alignment.center,
                child: Text(
                  title,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      fontFamily: "Poppins",
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Color.fromARGB(255, 0, 0, 0)),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FittedBox(
                      alignment: Alignment.center,
                      // fit: BoxFit.contain,
                      child: Container(
                          margin: const EdgeInsets.only(top: 15),
                          padding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 13),
                          decoration: BoxDecoration(
                            color: blueLight,
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Text(
                            category,
                            style: seeMoreText,
                            textAlign: TextAlign.center,
                          ))),
                  Container(
                      margin: const EdgeInsets.only(top: 10, left: 20),
                      alignment: Alignment.center,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                                margin: const EdgeInsets.only(right: 10),
                                child: Image.asset("assets/icon/star.png",
                                    width: 15)),
                            Text(
                              rating.toString(),
                              style: const TextStyle(
                                  fontFamily: "Poppins",
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600),
                            ),
                          ])),
                ],
              ),
              Container(
                  margin: const EdgeInsets.only(top: 30),
                  padding:
                      const EdgeInsets.symmetric(vertical: 25, horizontal: 25),
                  decoration: BoxDecoration(
                    color: backgroundColor,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25)),
                    boxShadow: const [
                      BoxShadow(
                          color: Colors.black38,
                          spreadRadius: 0,
                          blurRadius: 1),
                    ],
                  ),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            "Total Payment",
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontSize: 16,
                                fontWeight: FontWeight.w700),
                          ),
                          buy == true
                              ? Text(
                                  "You already have this!",
                                  style: TextStyle(
                                      fontFamily: "Poppins",
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      color: blueDark),
                                )
                              : price == 0
                                  ? Text(
                                      "Free",
                                      style: TextStyle(
                                          fontFamily: "Poppins",
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          color: blueDark),
                                    )
                                  : Text(
                                      "Rp" + price.toString() + ",-",
                                      style: TextStyle(
                                          fontFamily: "Poppins",
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          color: blueDark),
                                    )
                        ],
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 30),
                        height: 325,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: const EdgeInsets.only(
                                  bottom: 20, left: 40, right: 40),
                              child: Text(
                                "Are you sure you want to buy this book?",
                                style: paymentText,
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Container(
                              child: dropDownMenu(),
                            ),
                            selectedValue.toString() == "ID_OVO"
                                ? Container(
                                    width: 300,
                                    margin: const EdgeInsets.only(bottom: 20),
                                    child: TextField(
                                      onChanged: onChangePhone,
                                      decoration: InputDecoration(
                                          errorText: phoneNumber != ''
                                              ? null
                                              : "*Required",
                                          hintText: "Phone Number",
                                          hintStyle: const TextStyle(
                                              color: Colors.grey),
                                          enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: const BorderSide(
                                                  color: Color.fromARGB(
                                                      255, 243, 241, 241),
                                                  width: 0)),
                                          fillColor: const Color.fromARGB(
                                              255, 243, 241, 241),
                                          filled: true),
                                    ),
                                  )
                                : const SizedBox(
                                    height: 5,
                                  ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(right: 5),
                                  child: RaisedButton(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 12, horizontal: 30),
                                      shape: RoundedRectangleBorder(
                                        side: BorderSide(color: blueDark),
                                        borderRadius:
                                            BorderRadius.circular(30.0),
                                      ),
                                      onPressed: () {
                                        phoneNumber.isEmpty &&
                                                selectedValue.toString() ==
                                                    "ID_OVO"
                                            ? showDialog(
                                                context: context,
                                                builder: (BuildContext ctx) =>
                                                    AlertDialog(
                                                      title: const Text(
                                                          "Phone Number is Required."),
                                                      content: const Text(
                                                          "You must fill in your phone number to make transactions using ovo."),
                                                      actions: <Widget>[
                                                        TextButton(
                                                            onPressed: () =>
                                                                Navigator.pop(
                                                                    ctx),
                                                            child: const Text(
                                                                "Ok"))
                                                      ],
                                                    ))
                                            : buyBook();
                                      },
                                      color: blueDark,
                                      textColor: Colors.white,
                                      child: Text('Buy now',
                                          style: buttonPaymentText)),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 5),
                                  child: RaisedButton(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 12, horizontal: 30),
                                      shape: RoundedRectangleBorder(
                                        side: BorderSide(color: blueDark),
                                        borderRadius:
                                            BorderRadius.circular(30.0),
                                      ),
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetailBook(index: index)));
                                      },
                                      color: Colors.white,
                                      textColor: const Color.fromARGB(
                                          255, 253, 253, 253),
                                      child: Text('Cancel',
                                          style: buttonPaymentText)),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ))
            ]))
      ]);
    }

    return Scaffold(
      backgroundColor: const Color(0xFFF9F8F8),
      appBar: AppBar(
          automaticallyImplyLeading: false,
          toolbarHeight: 100,
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                flex: 5,
                child: Container(
                    alignment: Alignment.center,
                    child: const Text(
                      "Confirm Payment",
                      textAlign: TextAlign.center,
                      maxLines: 2,
                      style: TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    )),
              )
            ],
          )),
      body: payment(),
    ); // This trailing comma makes auto-formatting nicer for build method.
  }
}
