// ignore_for_file: file_names
import 'package:flutter/material.dart';
import 'package:splash_screen_view/SplashScreenView.dart';

import '../register/register_page.dart';

class Splash extends StatelessWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromRGBO(0, 146, 255, 1),
            Color.fromRGBO(118, 171, 223, 1),
          ],
        )),
        child: SplashScreenView(
          navigateRoute: const RegisterPage(),
          duration: 5000,
          backgroundColor: Colors.transparent,
          text: "Ensiklopedi.id",
          textType: TextType.TyperAnimatedText,
          textStyle: const TextStyle(
              color: Colors.white,
              fontSize: 33,
              fontWeight: FontWeight.w700,
              fontFamily: 'Poppins'),
        ),
      ),
    ));
  }
}
