import 'package:flutter/material.dart';
import 'package:client/constants.dart';

class LoginInputController {
  late bool Function() isValid;
  late Map<String, String> Function() getvalues;
}

class InputField extends StatefulWidget {
  final LoginInputController controller;
  const InputField({Key? key, required this.controller}) : super(key: key);
  @override

  // ignore: no_logic_in_create_state
  State<InputField> createState() => _InputFieldState(controller);
}

class _InputFieldState extends State<InputField> {
  _InputFieldState(LoginInputController _controller) {
    _controller.isValid = isValid;
    _controller.getvalues = getValues;
  }

  String email = "";
  String password = "";
  String passwordErr = "";

  var isEmailValid = true;
  void onEmailChange(String newValue) {
    email = newValue;
    setEmailValidator(RegExp(EMAIL_REGEX).hasMatch(email));
  }

  void onPasswordChange(String newValue) {
    password = newValue;
    if (password.length < 8) {
      setState(() {
        passwordErr = "Password must be 8 characters long or more";
      });
    } else {
      setState(() {
        passwordErr = "";
      });
    }
  }

  void setEmailValidator(bool valid) {
    setState(() {
      isEmailValid = valid;
    });
  }

  bool isValid() {
    return isEmailValid &&
        password.isNotEmpty &&
        email.isNotEmpty &&
        password.length >= 8;
  }

  Map<String, String> getValues() {
    return {"email": email, "password": password};
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        const Align(
          alignment: Alignment.centerLeft,
          child: Text("Email",
              style: TextStyle(
                  fontSize: 13,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600)),
        ),
        TextField(
          onChanged: onEmailChange,
          decoration: InputDecoration(
              errorText: isEmailValid ? null : "invalid email",
              hintText: "example@gmail.com",
              hintStyle: const TextStyle(
                  color: Color.fromARGB(255, 158, 158, 158), fontSize: 13),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: const BorderSide(
                      color: Color.fromARGB(255, 243, 241, 241), width: 0)),
              fillColor: const Color.fromARGB(255, 243, 241, 241),
              filled: true),
        ),
        const SizedBox(
          height: 20,
        ),
        const Align(
          alignment: Alignment.centerLeft,
          child: Text("Password",
              style: TextStyle(
                  fontSize: 13,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600)),
        ),
        TextField(
          onChanged: onPasswordChange,
          obscureText: true,
          obscuringCharacter: "*",
          decoration: InputDecoration(
              hintText: "........",
              errorText: passwordErr.isNotEmpty ? passwordErr : null,
              hintStyle: const TextStyle(color: Colors.grey),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: const BorderSide(
                      color: Color.fromARGB(255, 243, 241, 241), width: 0)),
              fillColor: const Color.fromARGB(255, 243, 241, 241),
              filled: true),
        ),
      ],
    );
  }
}
