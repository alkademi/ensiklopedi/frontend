import 'package:client/api_service/exception.dart';
import 'package:client/api_service/user_service.dart';
import 'package:client/constants.dart';
import 'package:client/screens/home/bottom_navbar.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'button_log.dart';
import 'input_field.dart';

class InputWrapper extends StatelessWidget {
  final LoginInputController controller = LoginInputController();
  final UserService userService = UserService();

  InputWrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    login() async {
      try {
        if (controller.isValid()) {
          var body = controller.getvalues();
          final response = await userService.login(
              body["email"] ?? "", body["password"] ?? "");
          if (response.data.token.isNotEmpty) {
            showDialog(
                context: context,
                builder: (BuildContext ctx) {
                  return AlertDialog(
                    title: const Text("Success"),
                    content: const Text("You have successfully logged in!"),
                    actions: <Widget>[
                      TextButton(
                        child: const Text("OK"),
                        onPressed: () {
                          Navigator.of(ctx).pop();
                          Navigator.of(context).pushReplacement(
                              MaterialPageRoute(builder: (BuildContext ctx) {
                            return BottomNavbar();
                          }));
                        },
                      )
                    ],
                  );
                });
          }
          var pref = await SharedPreferences.getInstance();
          pref.setInt("userData", response.data.user.id);
        }
      } on ServiceException catch (e) {
        String message = e.cause["message"] ?? "";
        showDialog(
            context: context,
            builder: (BuildContext ctx) => AlertDialog(
                  title: const Text("Error"),
                  content: Text(message),
                  actions: <Widget>[
                    TextButton(
                        onPressed: () => Navigator.pop(ctx),
                        child: const Text("Ok"))
                  ],
                ));
      }
    }

    return Padding(
      padding: const EdgeInsets.all(30),
      child: Column(
        children: <Widget>[
          const SizedBox(
            height: 40,
          ),
          const Icon(
            Icons.account_circle_outlined,
            color: Colors.blue,
            size: 110.0,
          ),
          const SizedBox(
            height: 40,
          ),
          Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(10)),
            child: InputField(
              controller: controller,
            ),
          ),
          const SizedBox(
            height: 40,
          ),
          Button(onPressed: login),
          const SizedBox(
            height: 20,
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            const Text(
              "Don't have an account?",
              style: TextStyle(color: Colors.grey),
            ),
            InkWell(
                onTap: () {
                  Navigator.pushNamed(context, registerRoute);
                },
                child: Container(
                  margin: const EdgeInsets.only(left: 10),
                  child: GestureDetector(
                    child: const Text(
                      "Register",
                      style: TextStyle(color: Colors.blue),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, registerRoute);
                    },
                  ),
                ))
          ])
        ],
      ),
    );
  }
}
